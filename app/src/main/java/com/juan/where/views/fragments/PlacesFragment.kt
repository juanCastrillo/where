package com.juan.where.views.fragments

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.juan.where.R
import com.juan.where.utils.Utils
import com.juan.where.utils.toGoogleMapsUrl
import com.juan.where.utils.toLatLng
import com.juan.where.storage.entities.Place
import com.juan.where.viewmodels.PlacesViewModel
import java.lang.Exception
import kotlin.concurrent.thread
import com.juan.where.views.PlaceActivity
import com.juan.where.views.MainActivity
import android.app.ActivityOptions
import android.widget.ImageView
import com.google.android.gms.maps.model.LatLng


/**
 * Shows a list with all the Places
 * Each row has all the metadata of the place
 */
class PlacesFragment: Fragment() {

    private lateinit var placesRecyclerView: RecyclerView
    private lateinit var adapter:PlacesListAdapter
    private var currentLocation:LatLng = LatLng(0.0,0.0)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_places, container, false)

        getViewModel().getCurrentLocation().observe(this, Observer {
            currentLocation = it.toLatLng()
            adapter.reloadPosition(currentLocation)
        })

        var type = arguments?.getString("type")
        if(type == null) type = "closePlaces"

        when(type){
            "allPlaces" -> getViewModel().getPlaces().observe(this, Observer<List<Place>> {
                               adapter.reloadList(it)
                           })
            "closePlaces" -> getViewModel().getClosePlaces(currentLocation).observe(this, Observer<List<Place>> {
                                 adapter.reloadList(it)
                             })
            else -> getViewModel().getPlacesByCategory(type).observe(this, Observer<List<Place>> {
                        adapter.reloadList(it)
                    })
        }

        placesRecyclerView = v.findViewById(R.id.locationsList)

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this.context)
        placesRecyclerView.layoutManager = layoutManager

        /*val dividerItemDecoration = DividerItemDecoration(
            placesRecyclerView.context,
            DividerItemDecoration.VERTICAL
        )
        placesRecyclerView.addItemDecoration(dividerItemDecoration)*/

        adapter = PlacesListAdapter(
            this,
            object : PlacesListAdapter.ClickyPlacesListener {
                override fun changeToCategory(category: String) {
                    (activity as MainActivity).categoryChosen(category)
                }

                override fun openPlace(place: Place, view:ImageView) {
                    startPlaceActivity(place, view)
                }

                override fun requestImage(imageName: String): Bitmap? = getViewModel().getImage(imageName)

                override fun sharePlace(place: Place){

                    try {
                        val sharingString = "${place.name}\n${place.description}\n${place.toGoogleMapsUrl()}"

                        val sendIntent: Intent = Intent().apply {
                            action = Intent.ACTION_SEND
                            putExtra(Intent.EXTRA_TEXT, sharingString)
                            type = "text/plain"
                        }
                        startActivity(sendIntent)
                    }catch (e:Exception){Log.d("sharingLocationExcp", e.toString())}
                }

                override fun removePlace(place: Place, pos: Int) {
                    thread {
                        getViewModel().removePlace(place)
                        Utils.runOnUiThread(Runnable{
                            adapter.notifyItemRemoved(pos)
                        })
                    }
                }

                //Not reusable enough try something else
                override fun editPlace(place: Place, pos: Int) {
                    //Log.d("location", location.toString())
                    (activity as MainActivity).startEditingPlace(place)
                }

                override fun goToPlace(place: Place) { searchPlace(place) }

            }, this.context!!)

        placesRecyclerView.adapter = adapter

        return v
    }

    private fun startPlaceActivity(place: Place, view:View) {
        val intent = Intent(this.context, PlaceActivity::class.java)
        intent.putExtra("placeId", place.id)

        if(place.imageName != "") {
            val transitionActivityOptions =
                ActivityOptions.makeSceneTransitionAnimation(activity, view, "placeImage")

            startActivity(intent, transitionActivityOptions.toBundle())
        } else startActivity(intent)
    }

    /**
     * @param place the place to be searched
     * Opens google maps in the adequate given location
     */
    fun searchPlace(place: Place) {

        val mapIntent = getViewModel().getMapIntent(place)
        if (mapIntent.resolveActivity(activity!!.packageManager) != null)
            startActivity(mapIntent)
    }

    /**
     * -MainActivity
     * It rolls the list to the start
     * @Future, update list of data
     */
    fun reloadData(){
        placesRecyclerView.smoothScrollToPosition(0)
    }

    /**
     * It rolls the list to a position
     */
    fun rollToPlace(position: Int = 0){
        placesRecyclerView.smoothScrollToPosition(position)
    }

    fun getViewModel() = ViewModelProviders.of(activity!!).get(PlacesViewModel::class.java)

    companion object {
        fun newInstance() = PlacesFragment()
    }
}