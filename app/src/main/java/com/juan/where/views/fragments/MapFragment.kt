package com.juan.where.views.fragments

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.juan.where.R
import com.juan.where.utils.CalculateDayNight
import com.juan.where.utils.DayTime
import com.juan.where.utils.DayTime.*
import com.juan.where.storage.entities.Place
import com.juan.where.utils.toLatLng
import com.juan.where.viewmodels.PlacesViewModel
import java.lang.Exception
import android.graphics.Bitmap
import com.juan.where.utils.Utils
import kotlin.concurrent.thread


/**
 * Fragment that shows a map and given a list of location,
 * it would mark them on the map
 */
class MapFragment: Fragment(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private var markerList:MutableList<Marker> = mutableListOf()
    private lateinit var mapFragment:SupportMapFragment
    private lateinit var dayTime: DayTime
    private var colorChanged = false
    var locationMarked:LatLng? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_map, container,false)

        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        getViewModel().getCurrentLocation().observe(this, Observer {
            if(::mMap.isInitialized) zoomIntoPlace(it.toLatLng())
        })

        val id = arguments!!.getLong("Get")

        when(id) {
            0L-> getViewModel().getPlaces().observe(this, Observer<List<Place>> {
                    markMap(it)
                })
            else-> getViewModel().getPlace(id).observe(this, Observer<Place> {
                    addMark(it)
                })
        }

        return v
    }

    private fun getViewModel() = ViewModelProviders.of(activity!!).get(PlacesViewModel::class.java)

    /**
     * @param googleMap
     * Sets the map
     * Determine time of day and sets a theme depending on it
     * @Future should zoom into a more confortable view
     */
    //TODO mark user currentLocation
    override fun onMapReady(googleMap: GoogleMap) {

        mMap = googleMap

        val styleInt: Int

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
            val currentNightMode = this.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
            styleInt = when (currentNightMode) {
                Configuration.UI_MODE_NIGHT_NO -> R.raw.mapstyle_light // Night mode is not active, we're using the light theme
                Configuration.UI_MODE_NIGHT_YES -> R.raw.mapstyle_dark // Night mode is active, we're using dark theme
                else -> {throw NoSuchMethodException()}
            }
        }
        else {
            dayTime = CalculateDayNight()
            styleInt = when (dayTime) {

                DAY -> R.raw.mapstyle_light
                NIGHT -> R.raw.mapstyle_dark
            }



        }

//        colorChanged = true


        if (!colorChanged) mMap.setMapStyle(
            MapStyleOptions.loadRawResourceStyle(
                this.context!!,
                styleInt
            )
        )
        //markMap()

        mMap.setOnMarkerClickListener {
            false//(activity as MainActivity).openPlacesListFragment(markerList.indexOf(it))
        }
        mMap.setOnMapClickListener { //locationMarked = it; Log.d("locationMarked", locationMarked.toString())
             }

        //if(currentLocation != LatLng(0.0,0.0)) mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15F))
    }

    private fun customMarker(baseImage: Bitmap):BitmapDescriptor{

        /*val view = (this.context!!) as LayoutInflater).inflate(
            R.layout.view_custom_marker,
            null
        )
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight())
        view.buildDrawingCache()
        val returnedBitmap = Bitmap.createBitmap(
            view.getMeasuredWidth(), view.getMeasuredHeight(),
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(returnedBitmap)
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
        val drawable = view.getBackground()
        drawable?.draw(canvas)
        view.draw(canvas)*/
        return BitmapDescriptorFactory.fromBitmap(null)

    }

    /**
     * @param location
     * @param name
     * Method to add a marker and zoom into it
     */
    fun addMark(place:Place){

        markerList.add(mMap.addMarker(MarkerOptions().position(place.toLatLng()).title(place.name)))
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.toLatLng(), 18F))
    }

    /**
     * @param place
     * remove a mark from the map
     */
    fun removeMark(position:Int) {
        markerList[position].remove()
    }

    /**
     * Given the list of places it adds all of the location on
     * the map as markers
     */
    private fun markMap(placesList:List<Place>) {

        clearMap()

        var i = 0

        thread {
            placesList.forEach {
                Log.d("markerx", "${it.latitude}, ${it.longitude}")

                Utils.runOnUiThread(Runnable {
                    try {
                        markerList.add(
                            i,

                            mMap.addMarker(
                                MarkerOptions()
                                    .position(LatLng(it.latitude, it.longitude)).title(it.name)
                                //.icon(customMarker(getImageFromStorage(it.imageName)!!))
                            )
                        ); i++
                    } catch (e: Exception) { Log.d("Couldn'tAddMarker", e.toString()) }
                })

                Thread.sleep(500)
            }
        }
    }

    private fun clearMap(){

        if(markerList.size > 0) {
            markerList.forEach { it.remove() }
            markerList = mutableListOf()
        }
    }

    /**
     * Toggle between the 2 styles of map
     */
    fun toggleMapStyle(){
        dayTime = if(dayTime == DAY) NIGHT else DAY

        val styleInt = when(dayTime){
            DAY -> R.raw.mapstyle_light
            NIGHT -> R.raw.mapstyle_dark
        }

        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this.context!!, styleInt))
    }

    fun zoomIntoPlace(pos:LatLng) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 16F))
    }

    companion object {
        fun newInstance() = MapFragment()
    }

}