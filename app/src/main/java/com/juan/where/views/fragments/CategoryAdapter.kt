package com.juan.where.views.fragments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.juan.where.R

class CategoryAdapter(
    var listener:CategoryListListener
): RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var categoriesList:List<String> = listOf()

    fun setCategoriesList(list:List<String>){
        categoriesList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val categoryName = categoriesList[position]
//        holder.categoryName.text = categoryName
        holder.wholeThing.setOnClickListener { listener.categoryClick(categoryName) }
        holder.wholeThing.text = categoryName
    }

    override fun getItemCount(): Int = categoriesList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.category, parent, false) as View
        return CategoryViewHolder(view)
    }


    class CategoryViewHolder(v: View):RecyclerView.ViewHolder(v) {

        var wholeThing: Chip
//        var categoryName:TextView

        init {
            wholeThing = v.findViewById(R.id.wholeThing)
//            categoryName = v.findViewById(R.id.categoryName)
        }

    }

    interface CategoryListListener {
        fun categoryClick(categoryName:String)
    }

}