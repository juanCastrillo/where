package com.juan.where.views.fragments

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.PermissionChecker
import androidx.core.content.PermissionChecker.checkSelfPermission
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.button.MaterialButton
import com.juan.where.*
import com.juan.where.utils.*
import com.juan.where.storage.entities.Place
import com.juan.where.viewmodels.PlacesViewModel
import java.io.File
import kotlin.concurrent.thread


/**
 * BottomSheetDialogFragment that comes from MainActivity
 * Its sole purpose its for the user to input data about a Place
 * Has 2 textBox and a imageGetter
 */
class AddPlaceBottomFragment: BottomSheetDialogFragment() {


    private lateinit var editTextName:EditText
    private lateinit var editTextDescription:EditText
    private lateinit var realLocationBloc:LinearLayout
    private lateinit var realLocationName:TextView
    private lateinit var getImageBloc:LinearLayout
    private lateinit var getCameraImageButton: LinearLayout
    private lateinit var getGalleryImageButton: LinearLayout
    private lateinit var placeImage: ImageView
    private lateinit var categorySpinner: Spinner
    private lateinit var categoryEditText: EditText

    private lateinit var place: Place
    //private var imageOfLocation:Bitmap? = null
    private lateinit var categoryList:MutableList<String>

    private lateinit var addButton: MaterialButton

    private val START_GALLERY = 101
    private val START_CAMERA = 102

    //TODO add more than 1 category
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.bottomfragment_add_marker, container, false)

        val id:Long? = arguments?.getLong("id")
        val location: LatLng? = arguments?.getParcelable("location")

        editTextName = view.findViewById(R.id.editTextName)
        editTextDescription = view.findViewById(R.id.editTextDescription)
        realLocationBloc = view.findViewById(R.id.realLocationBloc)
        addButton = view.findViewById(R.id.addPlaceButton)
        placeImage = view.findViewById(R.id.placeImage)
        getImageBloc = view.findViewById(R.id.getImageBloc)
        getGalleryImageButton = view.findViewById(R.id.getGalleryImageButton)
        getGalleryImageButton.setOnClickListener { getGalleryImageButton() }
        getCameraImageButton = view.findViewById(R.id.getCameraImageButton)
        getCameraImageButton.setOnClickListener { getCameraImageButton() }

        categorySpinner = view.findViewById(R.id.categorySpinner)
        categoryEditText = view.findViewById(R.id.newCatergoryEditText)

        realLocationName = view.findViewById(R.id.realLocationName)

        getViewModel().getCategories().observe(this, Observer {
            categoryList = it as MutableList<String>
            categoryList.add(resources.getString(R.string.not_selected))
            categoryList.reverse()
            val adapter = ArrayAdapter<String>(this.context!!,android.R.layout.simple_spinner_item, it)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            categorySpinner.adapter = adapter
        })

        categorySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {

                place.category = categoryList[position]
                categoryEditText.text.clear()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {}
        }

        place = Place()

        if(id != null && id != 0L) {
            Log.d("fuckingIdValue", id.toString()+"k")
            getViewModel().getPlace(id).observe(this, Observer {
                place = it
                updateThingsOnChange()
            })
        }


        //TODO ALLOW EDITING OF LOCATION PRESING ON TOP
        if(location != null) {
            Log.d("locationReceived", location.toString())
            place.latitude = location.latitude
            place.longitude = location.longitude
            setRealPlaceName(location)
        }

        else {
            getViewModel().getCurrentLocation().observe(this, Observer {

                Log.d("changeOfLocation", it.toString())
                if (place.toLatLng() == LatLng(0.0, 0.0)) {

                    place.latitude = it.latitude
                    place.longitude = it.longitude

                    //addButton.isEnabled = true

                    setRealPlaceName(place.toLatLng())
                }
            })
        }

        placeImage.setOnClickListener {
            place.imageName = ""
            placeImage.visibility = View.GONE
            getImageBloc.visibility = View.VISIBLE
        }

        //addButton.isEnabled = false
        addButton.setOnClickListener {

            place.name = editTextName.text.toString()
            place.description = editTextDescription.text.toString()
            place.imageName = place.imageName
            if(categorySpinner.selectedItemPosition==0) {
                if (!categoryEditText.text.isEmpty()) place.category = categoryEditText.text.toString()
                else place.category = ""
            }

            Log.d("PlaceAdd", place.toString()+"k")
            thread {
                if(getViewModel().setPlace(place)) dismiss()
                else Utils.runOnUiThread(Runnable { Toast.makeText(this.context, "Couldn't add, location already exists", Toast.LENGTH_LONG).show() })
            }
        }

        return view
    }

    /**
     * Set the global variable for the imageFileName
     * Checks permissions for camera and externalStorage
     * @param imageFileName name of the file for the image
     */
    private fun getGalleryImageButton(){
        //Set the image name for the current place
        place.imageName = getViewModel().createImageName()

        //Checks for permission
        if (checkSelfPermission(this.context!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED)
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 101) //

        else startGalleryForResult() //Permissions granted, open the gallery
    }

    /**
     * Event when the user presses the getCameraImageButton
     * sets the imageName, checks permissions and starts the intent
     */
    private fun getCameraImageButton() {
        place.imageName = getViewModel().createImageName()

        //Checks for permissions
        if (checkSelfPermission(this.context!!, Manifest.permission.CAMERA) != PermissionChecker.PERMISSION_GRANTED
            || checkSelfPermission(this.context!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED)
            requestPermissions(arrayOf(Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE), 102)

        else startCameraForResult() //Permissions granted, open the camera intent
    }

    /**
     * Gets the real name of the location.
     * Sets the name to the realLocationName
     * @param location
     */
    private fun setRealPlaceName(location:LatLng) {
        thread {
            try {
                place.realName = getViewModel().getLocationRealName(location)
            } catch (e: Exception) { Log.d("placeNameException", e.toString())}

            Utils.runOnUiThread(Runnable {
                realLocationBloc.visibility = View.VISIBLE
                realLocationName.text = place.realName
            })
        }
    }

    /**
     * Add to the view the data given by the viewmodel
     */
    private fun updateThingsOnChange() {
        editTextName.append(place.name)
        editTextDescription.append(place.description)

        if (place.imageName != "") {
            val image = getViewModel().getImage(place.imageName)
            if (image != null) setLocationImage(image)
        }
    }

    /**
     * @return the activity's PlacesViewModel
     */
    private fun getViewModel():PlacesViewModel = ViewModelProviders.of(activity!!).get(PlacesViewModel::class.java)

    /**
     * Creates an intent for the camera with the file to be saved
     * and starts the camera app.
     */
    private fun startCameraForResult(){
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (cameraIntent.resolveActivity(activity!!.packageManager) != null) {

            var photoFile: File? = null
            try {

                createPhotoDirectory()
                photoFile = File(File(directory), "${place.imageName}.jpeg")
                //currentImageLocation = "file:${photoFile.absolutePath}"

            } catch (ex: Exception) { Log.i("StartCameraForResult", ex.toString()) }

            if (photoFile != null) {
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile))
                cameraIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                startActivityForResult(cameraIntent, 1)
            }
        }
    }

    private fun startGalleryForResult(){
        try {
            val photoPickerIntent = Intent(Intent.ACTION_PICK)
            photoPickerIntent.type = "image/*"
            startActivityForResult(photoPickerIntent, 2)
        } catch (e:Exception) {Toast.makeText(context, resources.getText(R.string.gallery_not_found), Toast.LENGTH_LONG).show()}
//        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
//        intent.addCategory(Intent.CATEGORY_OPENABLE)
//        intent.type = "image/*"
//        startActivityForResult(intent, 2)
    }

    override fun onRequestPermissionsResult(requestCode:Int, permissions:Array<String>, grantResults:IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(requestCode == START_GALLERY){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) startGalleryForResult()
            else Toast.makeText(this.context, "storage permission denied", Toast.LENGTH_LONG).show()
        }
        if(requestCode == START_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                startCameraForResult()
        }
    }

    override fun onActivityResult(requestCode:Int, resultCode:Int, data:Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        /**
         * takes the image return from the camera Intent and sets it in the fragment
         */
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            try {
                setLocationImage(
                    getViewModel().resizeBitmap(MediaStore.Images.Media.getBitmap(
                        activity!!.contentResolver,
                        Uri.parse("file:$directory/${place.imageName}.jpeg")
                    ))!!
                )
            } catch (e: Exception) {e.printStackTrace()}
        }

        /**
         * takes the image return from the gallery Intent and sets it in the fragment
         */
        if(requestCode == 2 && resultCode == Activity.RESULT_OK){
            if(data!= null) {

                val imageUri = data.data
                val pathImage = getBitmapPath(imageUri)
                Log.d("imageUri", pathImage)
                val imageStream = activity!!.contentResolver.openInputStream(imageUri!!)
                val selectedImage = BitmapFactory.decodeStream(imageStream)
                val path = getViewModel().separateNameFromPath(pathImage)
                val name = path.first
                if(getViewModel().setImage(selectedImage, name, path.second)) {
                    place.imageName = name
                    setLocationImage(getViewModel().resizeBitmap(selectedImage)!!)
                }

            }
        }
    }

    /**
     * @param image image of the place
     * Only called from MainActivity, given the image, sets it and quits button to take photo
     */
    fun setLocationImage(image: Bitmap) {
        //imageOfLocation = image
        getImageBloc.visibility = View.GONE
        placeImage.visibility = View.VISIBLE
        placeImage.setImageBitmap(image)
    }

    fun getBitmapPath(selectedImage: Uri?): String {
        val cursor = context!!.contentResolver.query(
            selectedImage!!,
            arrayOf(MediaStore.Images.ImageColumns.DATA),
            null,
            null,
            null
        )
        cursor!!.moveToFirst()

        val idx = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA)//MediaStore.Images.ImageColumns.DATA)
        val selectedImagePath = cursor.getString(idx)
        cursor.close()

        return selectedImagePath ?: ""
    }

    companion object {
        fun getInstance(): AddPlaceBottomFragment = AddPlaceBottomFragment()
    }
}


