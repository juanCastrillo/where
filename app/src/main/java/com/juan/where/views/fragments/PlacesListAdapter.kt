package com.juan.where.views.fragments

import android.content.Context
import android.graphics.Bitmap
import android.opengl.Visibility
import android.util.Log
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.chip.Chip
import com.juan.where.R
import com.juan.where.utils.distance
import com.juan.where.utils.toFormatDistance
import com.juan.where.utils.toLatLng
import com.juan.where.storage.entities.Place


/**
 * RecyclerViewAdapter for PlacesFragment recyclerview
 * contains a Place's
 * @name
 * @description
 * @image
 * @location
 */
class PlacesListAdapter(
    val fragment: Fragment,
    val listener: ClickyPlacesListener,
    val context: Context
): RecyclerView.Adapter<PlacesListAdapter.PlacesViewHolder>() {

    private var placesList: List<Place> = emptyList()
    private var currentPosition: LatLng = LatLng(0.0,0.0)

    //TODO implement petition when you scroll more than 10
    fun reloadList(newPlacesList: List<Place>) {
        placesList = newPlacesList
        notifyDataSetChanged()
        Log.d("ReloadingList", "k")
    }

    fun reloadPosition(pos:LatLng){
        if(currentPosition == LatLng(0.0,0.0)) {
            currentPosition = pos
            notifyDataSetChanged()
        }
    }

    override fun onBindViewHolder(holder: PlacesViewHolder, position: Int) {

        //Check if the place has a name to either keep it hide or show it
        if (placesList[position].name == "") holder.placeName.visibility = View.GONE
        else {
            holder.placeName.visibility = View.VISIBLE
            holder.placeName.text = placesList[position].name
        }

        //Check if the place has a description to either keep it hide or show it
        if (placesList[position].description == "") holder.placeDescription.visibility = View.GONE
        else {
            holder.placeDescription.visibility = View.VISIBLE
            holder.placeDescription.text = placesList[position].description
        }

        //This name always exists so its always set
        holder.realPlaceName.text = placesList[position].realName

        //Set an image if it exists
        val imageName = placesList[position].imageName
        if (imageName != "") {
            val image:Bitmap? = listener.requestImage(imageName)//getViewModel().getImage(imageName)
            if (image != null) {
                holder.placeImage.setImageBitmap(image)
                holder.placeImage.visibility = View.VISIBLE
            }
            else holder.placeImage.visibility = View.GONE
        }else holder.placeImage.visibility = View.GONE

        //Set events for clicks with the adapter
        holder.menuPlaceButton.setOnClickListener {
            val popup = PopupMenu(fragment.context, holder.menuPlaceButton)
            popup.inflate(R.menu.places_list_menu)
            popup.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.places_list_edit_menu -> {
                        listener.editPlace(placesList[position], position); true
                    }
                    R.id.places_list_delete_menu -> {
                        listener.removePlace(placesList[position], position); true
                    }
                    R.id.places_list_share_menu -> {
                        listener.sharePlace(placesList[position]); true
                    }
                    else -> false
                }
            }
            popup.show()
        }

        holder.placeDistanceFromCurrentLocation.visibility = View.GONE

        //TODO FIX DISTANCE
        val currentDistance = distance(placesList[position].toLatLng(), currentPosition)
            /*sqrt((
                (placesList[position].latitude - currentPosition.latitude).pow(2) +
                (placesList[position].longitude - currentPosition.longitude).pow(2)
            ))*/


        Log.d("myLocation", "${placesList[position].latitude} x ${placesList[position].longitude}")
        Log.d("placeLocation", "${currentPosition.latitude} x ${currentPosition.longitude}")
        Log.d("distanceLocation", currentDistance.toString())



        if(currentDistance > 0.05) {
            holder.placeDistanceFromCurrentLocation.visibility = View.VISIBLE
            holder.placeDistanceFromCurrentLocation.text = currentDistance.toFormatDistance()
        }

        val category = placesList[position].category
        if(category != "") {
            holder.categoryChip.visibility = View.VISIBLE
            holder.categoryChip.text = category
            holder.categoryChip.setOnClickListener { listener.changeToCategory(category) }
        }
        else holder.categoryChip.visibility = View.GONE

        holder.wholeCard.setOnClickListener { listener.openPlace(placesList[position], holder.placeImage) }
        holder.locationBloc.setOnClickListener { listener.goToPlace(placesList[position]) }

    }

    override fun getItemCount(): Int = placesList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlacesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.place, parent, false) as View
        return PlacesViewHolder(view)
    }

    class PlacesViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        val wholeCard: LinearLayout
        val menuPlaceButton: ImageButton
        val placeName: TextView
        var placeDescription: TextView
        var realPlaceName: TextView
        var placeImage: ImageView
        var locationBloc: LinearLayout
        var placeDistanceFromCurrentLocation: TextView
        var categoryChip:Chip
        //var placesListSeparator:View
        //var paddingBlock:LinearLayout


        init {
            wholeCard = v.findViewById(R.id.wholeLocation)
            menuPlaceButton = v.findViewById(R.id.menuPlaceButton)
            placeName = v.findViewById(R.id.locationName)
            placeDescription = v.findViewById(R.id.locationDescription)
            realPlaceName = v.findViewById(R.id.realPlaceName)
            placeImage = v.findViewById(R.id.locationImage)
            locationBloc = v.findViewById(R.id.locationBloc)
            placeDistanceFromCurrentLocation = v.findViewById(R.id.placeDistanceFromCurrentLocation)
            categoryChip = v.findViewById(R.id.categoryChip)
            //placesListSeparator = v.findViewById(R.id.placesListSeparator)
            //paddingBlock = v.findViewById(R.id.paddingBlock)
        }
    }

    interface ClickyPlacesListener {

        fun openPlace(place: Place, view:ImageView)

        fun goToPlace(place: Place)
        fun editPlace(
            place: Place,
            pos: Int
        )

        fun changeToCategory(category:String)

        fun removePlace(place: Place, pos: Int)
        fun sharePlace(place: Place)

        fun requestImage(imageName:String):Bitmap?
    }
}



