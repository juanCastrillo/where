package com.juan.where.views

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.button.MaterialButton
import com.juan.where.R
import com.juan.where.Start
import com.juan.where.utils.distance
import com.juan.where.utils.toFormatDistance
import com.juan.where.utils.toLatLng
import com.juan.where.storage.entities.Place
import com.juan.where.viewmodels.Factory
import com.juan.where.viewmodels.PlacesViewModel
import com.juan.where.views.fragments.AddPlaceBottomFragment
import com.juan.where.views.fragments.MapFragment
import kotlin.concurrent.thread


class PlaceActivity:AppCompatActivity() {

    private lateinit var barapp: AppBarLayout
    private lateinit var placeName: TextView
    private lateinit var placeDescription: TextView
    private lateinit var realPlaceName: TextView
    private lateinit var placeImage: ImageView
    private lateinit var locationBloc: LinearLayout
    private lateinit var editPlaceButton:MaterialButton
    private lateinit var distanceFromLocation: TextView

    private lateinit var placesViewModel: PlacesViewModel
    private lateinit var place: Place

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place)

        placesViewModel = ViewModelProviders.of(
            this@PlaceActivity,
            Factory((application as Start).placesRepository,application)
        ).get(PlacesViewModel::class.java)
        placesViewModel.initializeLocation(this)


        val id:Long? = intent.getLongExtra("placeId",0L)

        if(id == null || id == 0L) finish()

        placesViewModel.getPlace(id!!).observe(this, Observer {
            place = it
            updateThingsOnChange()
        })

        placesViewModel.getCurrentLocation().observe(this, Observer { updateDistance(it.toLatLng()) })

        barapp = findViewById(R.id.barapp)
        placeName = findViewById(R.id.locationName2)
        placeDescription = findViewById(R.id.locationDescription2)
        realPlaceName = findViewById(R.id.realPlaceName2)
        placeImage = findViewById(R.id.locationImage2)
        locationBloc = findViewById(R.id.locationBloc2)
        distanceFromLocation = findViewById(R.id.distanceFromLocation)
        editPlaceButton = findViewById(R.id.editPlaceButton)

        editPlaceButton.setOnClickListener {

            val addPlaceBottomFragment = AddPlaceBottomFragment.getInstance()
            val bundle = Bundle()
            bundle.putLong("id", id)
            addPlaceBottomFragment.arguments = bundle

            addPlaceBottomFragment.show(supportFragmentManager, null)
        }

        thread {
            instanciateMap(id)
        }
    }

    private fun updateDistance(currentLocation: LatLng) {
        if(::place.isInitialized) distanceFromLocation.text = distance(currentLocation, place.toLatLng()).toFormatDistance()
    }


    private fun instanciateMap(id:Long) {
        val map = MapFragment.newInstance()
        val bundle = Bundle()
        bundle.putLong("Get",id)
        map.arguments = bundle
        supportFragmentManager.beginTransaction().replace(R.id.mapFrame, map).commit()
    }

    private fun updateThingsOnChange() {
        placeName.text = place.name

        if(place.description == "") placeDescription.visibility = View.GONE
        else {
            placeDescription.visibility = View.VISIBLE
            placeDescription.text = place.description
        }

        if(place.imageName == ""){
            barapp.visibility = View.GONE
        }else {
            placeDescription.visibility = View.VISIBLE
            placeImage.setImageBitmap(placesViewModel.getImage(place.imageName))
        }
        if(place.realName == "") locationBloc.visibility = View.GONE
        else {
            locationBloc.visibility = View.VISIBLE
            realPlaceName.text = place.realName
        }

    }
}