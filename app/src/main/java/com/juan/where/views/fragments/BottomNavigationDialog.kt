package com.juan.where.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.juan.where.R
import com.juan.where.viewmodels.PlacesViewModel
import com.juan.where.views.MainActivity

//TODO fix shit design
class BottomNavigationDialog: BottomSheetDialogFragment() {

    private lateinit var categoriesList:RecyclerView
    private lateinit var adapter:CategoryAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.bottomfragment_navigation, container)

        getViewModel().getCategories().observe(this, Observer { adapter.setCategoriesList(it) })

        categoriesList = v.findViewById(R.id.categoriesList)

        val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(this.context, 2)
        categoriesList.layoutManager = layoutManager

        adapter = CategoryAdapter( object : CategoryAdapter.CategoryListListener {
                override fun categoryClick(categoryName: String) {
                    (activity as MainActivity).categoryChosen(categoryName)
                }
            })

        categoriesList.adapter = adapter

        return v
    }

    private fun getViewModel() = ViewModelProviders.of(activity!!).get(PlacesViewModel::class.java)

    companion object {
        fun newInstance() = BottomNavigationDialog()
    }

}