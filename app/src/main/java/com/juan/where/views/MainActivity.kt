package com.juan.where.views

import android.Manifest
import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.button.MaterialButton
import com.juan.where.views.fragments.AddPlaceBottomFragment
import com.juan.where.views.fragments.MapFragment
import com.juan.where.views.fragments.PlacesFragment
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Handler
import android.view.MenuItem

import androidx.lifecycle.ViewModelProviders
import com.juan.where.R
import com.juan.where.Start
import com.juan.where.viewmodels.PlacesViewModel
import com.juan.where.storage.entities.Place
import com.juan.where.viewmodels.Factory
import android.widget.Toast
import androidx.appcompat.widget.ActionMenuView
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.bottomappbar.BottomAppBar
import com.juan.where.R.id.bar
import com.juan.where.views.fragments.BottomNavigationDialog


/**
 * Activity which contains 2 fragments navigated by a Bottom Navigation
 *  {@link MapFragment}
 *  {@link PlacesFragment}
 *
 *  Manages the creation, allocation and modification of Places
 *  of the external storage
 */
class MainActivity: AppCompatActivity() {

    private var visibleFragment = 0

    private var placesFragment: PlacesFragment? = null
    private var mapFragment: MapFragment? = null
    private var addPlaceBottomFragment: AddPlaceBottomFragment? = null
    //private var bottomNavigationFragment: BottomNavigationDialog? = null
    private lateinit var bottomAppBar:BottomAppBar

    private lateinit var addPlaceButton:MaterialButton
    //private lateinit var bottomNavigationView:BottomNavigationView

    private val fm = supportFragmentManager


    private lateinit var placesViewModel: PlacesViewModel


    val PLACES_FRAGMENT_TAG = "places"
    val MAP_FRAGMENT_TAG ="map"
    private val SHORTCUT_ACTION = "create.place.from.shortcut"
    private val SHARING_RECEIVE_ACTION = "android.intent.action.SEND"
    val LOCATION_REQUEST = 2
    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        placesViewModel = ViewModelProviders.of(
            this@MainActivity,
            Factory((application as Start).placesRepository,application)
        ).get(PlacesViewModel::class.java)
        placesViewModel.initializeLocation(this)
        placesViewModel.startLocationUpdates()

        addPlaceButton = findViewById(R.id.addPlaceButton)

        bottomAppBar = findViewById(R.id.bar)
        bottomAppBar.setNavigationOnClickListener {
            //if(it.id == android.R.id.home)
            BottomNavigationDialog.newInstance().show(fm, null)
        }

        placesFragment = PlacesFragment.newInstance()
        openPlacesListFragment(null)//fm.beginTransaction().replace(R.id.mainFragmentCanvas, placesFragment!!, "places").commit()

        addPlaceButton.setOnClickListener {
            checkLocationAndOpen()
        }

        when(intent.action){
            SHORTCUT_ACTION -> addLocation()
            SHARING_RECEIVE_ACTION -> {
                intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
                    val location = placesViewModel.getRealNameLocation(it)
                    addLocation(location)
                }
            }
        }

    }

    private fun checkLocationAndOpen() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION))
                Toast.makeText(this, "Allow location permission", Toast.LENGTH_LONG).show()
            else ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST)
        }
        else addLocation()
    }

    /**
     * @param location
     * Creates a {@link AddPlaceBottomFragment} if theres none, and gives it a location
     */
    fun addLocation(location:LatLng? = null) {

        addPlaceBottomFragment = AddPlaceBottomFragment.getInstance()

        if (location != null){
            val bundle = Bundle()
            bundle.putParcelable("location", location)
            addPlaceBottomFragment?.arguments = bundle
        }

        addPlaceBottomFragment?.show(fm, null)
    }

    /**
     * -PlacesFragment
     * @param place
     * with all previous data from place, sends it to AddPlaceBottomFragment
     * to be manage
     */
    fun startEditingPlace(place: Place){

        addPlaceBottomFragment = AddPlaceBottomFragment.getInstance()

        val bundle = Bundle()
        bundle.putLong("id", place.id!!)

        addPlaceBottomFragment?.arguments = bundle
        addPlaceBottomFragment?.show(fm, null)
    }

    /**
     *
     */
    fun openMapFragment(position: Int?){

        if(visibleFragment == 1) mapFragment?.toggleMapStyle()
        else {

            if (mapFragment == null) {
                mapFragment = MapFragment.newInstance()
                val bundle = Bundle()
                bundle.putLong("Get", 0)
                mapFragment!!.arguments = bundle
                fm.beginTransaction().replace(R.id.mainFragmentCanvas, mapFragment!!, MAP_FRAGMENT_TAG).addToBackStack(MAP_FRAGMENT_TAG).commit()
            }

            else {
                val f = fm.findFragmentByTag(MAP_FRAGMENT_TAG)
                if (f != null) fm.beginTransaction().replace(R.id.mainFragmentCanvas, f, MAP_FRAGMENT_TAG).addToBackStack(MAP_FRAGMENT_TAG).commit()
                else fm.beginTransaction().replace(R.id.mainFragmentCanvas, mapFragment!!, MAP_FRAGMENT_TAG).addToBackStack(MAP_FRAGMENT_TAG).commit()
            }

            if(position != null) mapFragment?.zoomIntoPlace(LatLng(0.0,0.0))

            visibleFragment = 1
        }
    }

    /**
     *
     */
    fun openPlacesListFragment(position: Int?){
        

        //if(visibleFragment == 0) placesFragment?.reloadData()

        //else {

            if (placesFragment == null) {
                placesFragment = PlacesFragment.newInstance()
                fm.beginTransaction().replace(R.id.mainFragmentCanvas, placesFragment!!, PLACES_FRAGMENT_TAG).addToBackStack(PLACES_FRAGMENT_TAG).commit()
            }
            else {
                val f = fm.findFragmentByTag(PLACES_FRAGMENT_TAG)
                if (f != null) fm.beginTransaction().replace(R.id.mainFragmentCanvas, f, PLACES_FRAGMENT_TAG).addToBackStack(PLACES_FRAGMENT_TAG).commit()
                else fm.beginTransaction().replace(R.id.mainFragmentCanvas, placesFragment!!, PLACES_FRAGMENT_TAG).addToBackStack(PLACES_FRAGMENT_TAG).commit()
            }

            if(position != null) placesFragment?.rollToPlace(position)
            visibleFragment = 0
        //}
    }

    fun categoryChosen(category:String){

        if(placesFragment != null)
            placesFragment = null

        placesFragment =  PlacesFragment.newInstance()
        val bundle = Bundle()
        bundle.putString("type", category)
        placesFragment!!.arguments = bundle

        fm.beginTransaction().replace(R.id.mainFragmentCanvas, placesFragment!!, PLACES_FRAGMENT_TAG).addToBackStack(PLACES_FRAGMENT_TAG).commit()
    }

    override fun onResume() {
        super.onResume()
        placesViewModel.startLocationUpdates()
    }

    override fun onPause() {
        super.onPause()
        placesViewModel.stopLocationUpdates()
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, R.string.press_again_to_exit, Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            LOCATION_REQUEST -> {
                if(permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION) addLocation()
            }
        }
    }

}