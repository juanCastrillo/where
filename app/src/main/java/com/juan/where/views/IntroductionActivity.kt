package com.juan.where.views

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.button.MaterialButton
import com.juan.where.R

/**
 * Launcher Activity, handles nothing other than asking for location
 * user permission
 */
//todo fix
class IntroductionActivity: AppCompatActivity() {

    private lateinit var continueButton: MaterialButton

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        continuet()
        setContentView(R.layout.activity_introduction)

        if(locationPermission(this))
            continuet()

        continueButton = findViewById(R.id.continueButton)
        continueButton.setOnClickListener { continuet() }

    }

    fun locationPermission(activity: Activity):Boolean {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION))
                Toast.makeText(activity, "Allow location permission", Toast.LENGTH_LONG).show()
            else ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)

            return false
        }
        return true
    }

    private fun continuet(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}