package com.juan.where


import android.app.Application
import android.os.StrictMode
import com.juan.where.repositories.PlacesCacheRepository
import com.juan.where.repositories.PlacesRepository
import com.juan.where.repositories.PlacesRemoteRepository
import com.juan.where.storage.database.PlacesDataBase

class Start: Application() {

    lateinit var placesRepository: PlacesRepository
    lateinit var placesDataBase:PlacesDataBase

    override fun onCreate() {
        super.onCreate()

        placesDataBase = PlacesDataBase.getInstance(this)

        placesRepository = PlacesRepository.getInstance(
            PlacesRemoteRepository(),
            placesDataBase.placesDao(),
            PlacesCacheRepository()
        )

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
    }

}