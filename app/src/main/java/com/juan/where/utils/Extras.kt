package com.juan.where.utils

import android.location.Address
import android.location.Location
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.juan.where.storage.entities.Place
import java.text.SimpleDateFormat
import java.util.*
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks

class Utils {

    companion object {
        fun runOnUiThread(runnable: Runnable) {
            val uiHandler = Handler(Looper.getMainLooper())
            uiHandler.post(runnable)
        }
    }
}

fun Location.toLatLng(): LatLng = LatLng(this.latitude, this.longitude)
fun Place.toLatLng(): LatLng = LatLng(this.latitude, this.longitude)
fun Address.toLatLng():LatLng = LatLng(this.latitude, this.longitude)
fun Place.toGoogleMapsUrl(): Uri = Uri.parse("https://www.google.com/maps/search/?api=1&query="
        + this.realName.replace(" ", "+").replace(",", "")
)
fun Uri.customUrl(): Uri {
    val dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
        .setLink(this)
        .setDomainUriPrefix("https://Where")
        // Open links with this app on Android
        .setAndroidParameters(DynamicLink.AndroidParameters.Builder().build())
        .buildDynamicLink()

    return dynamicLink.uri
}

enum class DayTime {DAY, NIGHT}

fun CalculateDayNight(): DayTime {
    val time = Calendar.getInstance().time
    Log.d("time", time.toString())
    /*when(time){
        in 20.. -> {}
    }*/
    return DayTime.NIGHT
}

fun nowDate():String {
    val calendar = Calendar.getInstance()
    val mdformat = SimpleDateFormat("ymdHHmmss")
    return mdformat.format(calendar.time)
}

fun distance(plc1:LatLng, plc2:LatLng): Double {
    val k:FloatArray = FloatArray(1)
    Location.distanceBetween(plc1.latitude, plc1.longitude, plc2.latitude, plc2.longitude,k)

    return k[0].toDouble()
    /*val earthRadius = 3958.75
    val latDiff = Math.toRadians((plc2.latitude - plc1.latitude))
    val lngDiff = Math.toRadians((plc2.longitude - plc1.longitude))
    val a = Math.sin(latDiff / 2) * Math.sin(latDiff / 2) + Math.cos(Math.toRadians(plc1.latitude)) * Math.cos(
        Math.toRadians(plc2.latitude)
    ) *
            Math.sin(lngDiff / 2) * Math.sin(lngDiff / 2)
    val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    val distance = earthRadius * c

    val meterConversion = 1609

    return (distance * meterConversion)*/
}

fun Double.toFormatDistance() = if(this > 1000.0) "${String.format("%.2f", this/1000)} km"
else                         "${String.format("%.2f",this)} m"


/*maybe implement default streetview images
/**
 * gets image from streetview
 * and returns its url
 */
fun getImageUrl(location:Place):String{
    val queue = Volley.newRequestQueue(context)
    val sendUrl = "https://maps.googleapis.com/maps/api/streetview/metadata?size=600x300&location=${location.latitude},${location.longitude}"
    var receiveUrl = ""

    // Request a string response from the provided URL.
    val stringRequest = StringRequest(
        Request.Method.GET, sendUrl,
        Response.Listener<String> {
            // Display the first 500 characters of the response string.
            receiveUrl = it
        },
        Response.ErrorListener {})

    // Add the request to the RequestQueue.
    queue.add(stringRequest)

    return receiveUrl
}*/
