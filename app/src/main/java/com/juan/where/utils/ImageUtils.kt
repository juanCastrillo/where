package com.juan.where.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import java.io.File
import java.io.FileOutputStream

class ImageUtils {

    companion object {


        /**
         * @param imageToSave
         * @param fileName
         */
        fun saveImageToStorage(imageToSave: Bitmap, fileName: String): Boolean {

            if (fileName == "") return false

            val file = File(File(directory), "$fileName.jpeg")

            Log.d("directoryToSaveImage", file.absolutePath)

            createPhotoDirectory()

            return try {
                val out = FileOutputStream(file)
                imageToSave.compress(Bitmap.CompressFormat.JPEG, 95, out)
                out.flush()
                out.close()

                true

            } catch (e: Exception) {
                e.printStackTrace(); false
            }
        }

        /**
         * @param imageName the name for the image to be found in storage
         * Gets image from storage by its name
         */
        fun getImageFromStorage(imageName: String): Bitmap? {

            val pathToPicture = "$directory/$imageName.jpeg"
            var fullImg: Bitmap? = null
            try {
                fullImg = BitmapFactory.decodeFile(pathToPicture)
            } catch (e: Exception) {
                Log.e("nullImage", e.toString())
            }

            return fullImg
        }

        /**
         * Checks if a given file exists already in the given directory
         */
        fun checkFileInStorage(file: File) = file.exists()


    }
}

//TODO use a library to automatize the compression of images
fun Bitmap?.resize(maxSize: Int = 1200): Bitmap? {

    if (this == null) return null

    var width = this.width
    var height = this.height

    val bitmapRatio = width.toFloat() / height.toFloat()
    if (bitmapRatio > 1) {
        width = maxSize
        height = (width / bitmapRatio).toInt()
    } else {
        height = maxSize
        width = (height * bitmapRatio).toInt()
    }

    return Bitmap.createScaledBitmap(this, width, height, true)
}