package com.juan.where.utils

import java.io.File
import android.os.Environment

val directory = "${Environment.getExternalStorageDirectory().path}/Where/WherePhotos"
//val directory = "/sdcard/Where/WherePhotos"

fun createPhotoDirectory(){
    val direct = File("$directory/")

    if (!direct.exists()) {
        val photosDirectory = File("$directory/")
        photosDirectory.mkdirs()
    }
}

