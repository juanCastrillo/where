package com.juan.where.storage.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey

@Entity(tableName = "placesImages",
    foreignKeys = [ForeignKey(
        entity = Place::class, parentColumns = ["id"],
        childColumns = ["imageId"],
        onDelete = CASCADE
    )]
)
data class PlaceImage (
    @PrimaryKey(autoGenerate = true) val imageId: Long?,
    @ColumnInfo(name = "imageName") var imageName:String,
    @ColumnInfo(name = "imageDirectory") var imageDirectory:String,
    @ColumnInfo(name = "id") val id: Long?
) {constructor():this(null, "", "", null)}