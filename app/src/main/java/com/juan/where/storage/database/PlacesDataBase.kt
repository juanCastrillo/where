package com.juan.where.storage.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.juan.where.storage.daos.PlacesDao
import com.juan.where.storage.entities.Place
import com.juan.where.storage.entities.PlaceImage

@Database(entities = [Place::class, PlaceImage::class], version = 2)
abstract class PlacesDataBase:RoomDatabase() {
    abstract fun placesDao():PlacesDao

    companion object {
        private var INSTANCE: PlacesDataBase? = null

        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE places ADD category Text NOT NULL DEFAULT \"\"")
            }
        }

        val MIGRATION_2_3 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE places ADD category Text NOT NULL DEFAULT \"\"")
            }
        }


        fun getInstance(ct: Context): PlacesDataBase {
            if (INSTANCE == null) {
                synchronized(PlacesDataBase::class.java) {
                    INSTANCE = Room.databaseBuilder(
                        ct.applicationContext, PlacesDataBase::class.java, "places.db"
                    ).addMigrations(MIGRATION_1_2).build()
                }
            }
            return INSTANCE!!
        }

        fun destroyInstance(){ INSTANCE = null }
    }
}