package com.juan.where.storage.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.google.android.gms.maps.model.LatLng
import com.juan.where.storage.entities.Place

@Dao
interface PlacesDao {

    @Query("SELECT * FROM places")
    fun getAll():LiveData<List<Place>>

    @Query("SELECT * FROM places WHERE (latitude BETWEEN (:lat - 0.5) AND (:lat + 0.5) AND (longitude BETWEEN (:leng - 0.5) AND (:leng + 0.5)))")
    fun getClosePlaces(lat:Double, leng:Double):LiveData<List<Place>>

    @Query("SELECT * FROM places ORDER BY (((latitude-:lat)*(latitude-:lat))+((longitude-:leng)*(longitude-:leng))) LIMIT :number OFFSET :offset")
    fun getPlacesByDistance(lat:Double, leng:Double, number:Int = 10, offset:Int = 0):LiveData<List<Place>>

    @Query("SELECT * FROM places WHERE category = :category")
    fun getPlacesByCategory(category:String):LiveData<List<Place>>

    @Query("SELECT DISTINCT category FROM places WHERE category <> ''")
    fun getAllCategories():LiveData<List<String>>

    @Query("SELECT * FROM places WHERE id = :id")
    fun getPlaceAt(id:Long):LiveData<Place>

    @Insert(onConflict = REPLACE)
    fun insert(place: Place)

    @Query("DELETE FROM places")
    fun deleteAll()

    @Delete
    fun delete(vararg place: Place)
}