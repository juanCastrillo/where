package com.juan.where.storage.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "places")
data class Place(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "name") var name:String,
    @ColumnInfo(name = "latitude") var latitude:Double,
    @ColumnInfo(name = "longitude") var longitude:Double,
    @ColumnInfo(name = "description") var description:String,
    @ColumnInfo(name = "real_name") var realName:String,
    @ColumnInfo(name = "image_name") var imageName:String, //TODO - REMOVE and fix the queries to use PlaceImage entity
    @ColumnInfo(name = "category") var category:String

) {constructor():this(null, "", 0.0, 0.0, "", "", "", "")}