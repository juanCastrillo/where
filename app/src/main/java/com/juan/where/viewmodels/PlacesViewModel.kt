package com.juan.where.viewmodels

import android.Manifest
import android.app.Activity
import android.app.Application
import android.content.Intent
import android.graphics.Bitmap
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Looper
import android.util.Log
import androidx.core.content.PermissionChecker.checkPermission
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.juan.where.repositories.PlacesRepository
import com.juan.where.storage.entities.Place
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.juan.where.utils.nowDate
import com.juan.where.utils.resize
import com.juan.where.utils.toLatLng
import java.io.File

class PlacesViewModel(application: Application, private val placesRepository: PlacesRepository ) : AndroidViewModel(application) {

    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private lateinit var mSettingsClient: SettingsClient
    private lateinit var mLocationRequest: LocationRequest
    private lateinit var mLocationSettingsRequest: LocationSettingsRequest
    private lateinit var mLocationCallback: LocationCallback
    private var currentLocation: MutableLiveData<Location> = MutableLiveData()
    private var lastKnownLocation:LatLng = LatLng(0.0, 0.0)

    private var places:LiveData<List<Place>> = placesRepository.getPlaces()

    fun getPlaces() = places

    fun getPlace(id:Long) = placesRepository.getPlace(id)

    fun getClosePlaces(loc:LatLng) = placesRepository.getClosePlaces(loc)

    fun getPlacesByCategory(category:String) = placesRepository.getPlacesByCategory(category)

    fun setPlace(place: Place):Boolean = placesRepository.setPlace(place)

    fun removePlace(place: Place) { placesRepository.removePlace(place) }

    fun getCurrentLocation() = currentLocation

    fun getImage(imageName:String):Bitmap? = placesRepository.getImage(imageName)

    fun resizeBitmap(image: Bitmap?, size:Int = 1200) = image.resize(size)

    fun setImage(image:Bitmap, imageName: String, dir:String): Boolean = placesRepository.saveImage(image, imageName, dir)

    fun getCategories() = placesRepository.getCategories()

    /**
     * Creates a name for an image
     */
    fun createImageName() = "IMAGE${nowDate()}"

    /**
     * @param location coordinates of the place
     * Changes de coordinates to a real human street, city and country name
     * @return the name of the place redable
     */
    fun getLocationRealName(location:LatLng):String{
        val geocoder = Geocoder(getApplication())
        var adress: Address? = null
        try {
            val addressList = geocoder.getFromLocation(location.latitude, location.longitude, 1)
            adress = addressList[0]
        } catch (e:Exception ) { e.printStackTrace() }

        //Log.d("realPlaceNameXD", adress?.toString())

        return if(adress != null) adress.getAddressLine(0)
        else ""
    }

    fun getRealNameLocation(realName:String):LatLng?{
        val geocoder = Geocoder(getApplication())
        val i = geocoder.getFromLocationName(realName, 1)
        return i[0].toLatLng()
    }

    fun initializeLocation(activity: Activity){

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)
        mSettingsClient = LocationServices.getSettingsClient(activity)

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                if(locationResult!=null) {
                    lastKnownLocation = locationResult.locations[0].toLatLng()
                    currentLocation.value = locationResult.locations[0]
                    Log.d("newLocation", currentLocation.value.toString())
                }
            }
        }

        mLocationRequest = LocationRequest()
        mLocationRequest.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        mLocationSettingsRequest = builder.build()

        startLocationUpdates()
    }

    fun getMapIntent(place: Place):Intent {
        val gmmIntentUri = Uri.parse("geo:0,0?q=${place.latitude},${place.longitude}(${place.name})")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")

        return mapIntent
    }

    /**
     * @param loc location to be checked
     * Checks for a location in the list of locations thats 0.005
     * near the location given
     *
     * @return false if location is new true else
     */
    /*private fun checkRepeatedLocation(loc: LatLng):Boolean{
        places.value?.forEach {
            if(it.latitude in loc.latitude-0.005..loc.latitude+0.005)
                if(it.longitude in loc.longitude-0.005..loc.longitude+0.005)
                    return true
        }
        return false
    }*/

    /**
     * starts checking for updates on location
     */
    fun startLocationUpdates() {
        checkPermission(getApplication(), Manifest.permission.ACCESS_FINE_LOCATION, 0, 0, "com.juan.where")
        mFusedLocationClient.lastLocation.addOnSuccessListener {
            if (it != null) {
            Log.d("lastKnownLocation", it.toLatLng().toString())
            currentLocation.value = it} }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
            mLocationCallback,
            Looper.getMainLooper()
        )
    }

    /**
     * stops updating listener
     */
    fun stopLocationUpdates() { mFusedLocationClient.removeLocationUpdates(mLocationCallback) }

    fun separateNameFromPath(path:String): Pair<String, String> {

        var name = File(path).name
        val dir = path.replace("/$name", "")
        name = name .replace(".jpg", "")
            .replace(".jpeg","")
        return Pair(name, dir)
    }

    companion object {
        private const val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
        private const val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS: Long = 5000
    }

}

/**
 * This creator is to showcase how to inject dependencies into ViewModels. It's not
 * actually necessary in this case, as the BusRepository can be passed in a public method.
 */
class Factory(private val placesRepository: PlacesRepository, val application: Application) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        return PlacesViewModel(application, placesRepository) as T
    }
}