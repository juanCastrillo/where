package com.juan.where.repositories

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.juan.where.utils.createPhotoDirectory
import com.juan.where.utils.directory
import com.juan.where.storage.daos.PlacesDao
import com.juan.where.storage.entities.Place
import com.juan.where.utils.ImageUtils
import com.juan.where.utils.ImageUtils.Companion.getImageFromStorage
import com.juan.where.utils.resize
import java.io.File
import java.io.FileOutputStream

//TODO create livedata in here not in the repositories
class PlacesRepository(private val cacheRepository: PlacesCacheRepository,
                       private val remoteRepository: PlacesRemoteRepository,
                       private val placesDao: PlacesDao) {

    private var place = MutableLiveData<Place>()
    private var places = MutableLiveData<List<Place>>()


    /**
     * @param placeId
     * Gives the whole place in response for its id
     */
    fun getPlace(placeId: Long): LiveData<Place> {
        val cachedPlace = cacheRepository.getPlace(placeId)

        return if(cachedPlace != null) {
            place.value = cachedPlace
            place
        }
        else {
            placesDao.getPlaceAt(placeId)
        }

    }

    /**
     * Returns an observable for ALL the places table changes
     */
    fun getPlaces(): LiveData<List<Place>> {

        return placesDao.getAll()
    }

    fun getPlacesByCategory(category:String) = placesDao.getPlacesByCategory(category)

    fun getClosePlaces(loc: LatLng): LiveData<List<Place>> {
        return placesDao.getPlacesByDistance(loc.latitude, loc.longitude)
    }

    /**
     * @param place
     * Removes the given place
     */
    fun removePlace(place: Place) { placesDao.delete(place) }

    /**
     * @param place
     * Edits or adds the given place
     */
    fun setPlace(place: Place):Boolean{
        placesDao.insert(place)
        cacheRepository.setPlace(place)
        return true
    }

    /**
     * Checks if image is in cacheRepository if not
     * Gets image from storage and saves in cacheRepository
     */
    fun getImage(imageName:String): Bitmap? {
        val cacheImage = cacheRepository.getImage(imageName)
        return if(cacheImage != null) {
            cacheImage
        }
        else {
            val image = getImageFromStorage(imageName).resize()
            if(image != null) cacheRepository.newImage(imageName, image)
            image
        }
    }

    /**
     *
     */
    fun saveImage(image: Bitmap, imageName: String, dir: String = directory): Boolean {

        Log.d("imageName", imageName)
        Log.d("directory", directory)

        if (dir == directory) {
            val file = File(File(directory), "$imageName.jpeg")

            //if the file already exists dont save it
            if (ImageUtils.checkFileInStorage(file)) {
                return true
            }
        }

        //saved the file and return
        return ImageUtils.saveImageToStorage(image, imageName)

    }

    fun getCategories(): LiveData<List<String>> {

        return placesDao.getAllCategories()

    }

    companion object {

        private var sInstance: PlacesRepository? = null

        fun getInstance(remoteRepository: PlacesRemoteRepository, placesDao: PlacesDao, cacheRepository: PlacesCacheRepository): PlacesRepository {
            if (sInstance == null) {
                sInstance = PlacesRepository(cacheRepository, remoteRepository, placesDao)
            }
            return sInstance!!
        }
    }
}

