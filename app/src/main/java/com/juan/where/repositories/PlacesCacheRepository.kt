package com.juan.where.repositories

import android.graphics.Bitmap
import com.juan.where.storage.entities.Place

/**
 *
 */
//TODO change some types to liveData
class PlacesCacheRepository {

    private var placesList:MutableList<Place>
    private val placesImages:MutableMap<String,Bitmap>
    
    init {
        placesList = mutableListOf()
        placesImages = mutableMapOf()
    }

    fun newPlacesList(places:List<Place>) {
        placesList = places as MutableList<Place>
    }

    fun setPlace(place: Place) {
        val id = idOfPlaceInList(place.id)
        if(id != null) placesList[id] = place
        else placesList.add(place)
    }

    fun getPlace(id:Long):Place? {
        placesList.forEach { if(id == it.id) return it }
        return null
    }

    fun getPlacesList():List<Place> = placesList

    fun newImage(imageName: String, image:Bitmap):Boolean {
        placesImages[imageName] = image
        return true
    }

    fun getImage(imageName:String):Bitmap? = placesImages[imageName]

    private fun checkImageExists(imageName: String):Boolean =
        placesImages.containsKey(imageName)
    
    private fun idOfPlaceInList(id: Long?):Int?{

        placesList.forEach { if(id == it.id) return placesList.indexOf(it) }
        return null
    }

}