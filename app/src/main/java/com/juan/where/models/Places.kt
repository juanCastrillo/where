package com.juan.where.models

import android.graphics.Bitmap
import com.juan.where.storage.entities.Place

data class CompletePlace(
    var place: Place,
    var image: Bitmap?
)

//fun Place.toCompletePlace() = CompletePlace(this, getImageFromStorage(this.imageName)!!)
