package puntos.juan.where

import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.model.LatLng
import com.juan.where.Start
import com.juan.where.storage.entities.Place
import com.juan.where.viewmodels.Factory
import com.juan.where.viewmodels.PlacesViewModel
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class PlacesViewModelTests {

    //@Mock
    //val application:Start = mock(Start::class.java)
    val placesViewModel: PlacesViewModel = mock(PlacesViewModel::class.java)

    /*/init {
        placesViewModel = PlacesViewModel(
            application,
            application.placesRepository
        )
    }*/

    @Test
    fun getLocationRealName() {

        val location = LatLng(0.0,0.0)
        val realNameExpected = ""

        val realName = placesViewModel.getLocationRealName(location)
        assertEquals(realNameExpected, realName)
    }

    @Test
    fun getRealNameLocation() {
        val realName = ""
        val expectedLocation = LatLng(0.0,0.0)

        val location = placesViewModel.getRealNameLocation(realName)
        assertEquals(expectedLocation, location)
    }

    @Test
    fun getImage(){

        val imageName = ""

        val image = placesViewModel.getImage(imageName)!!
    }

    @Test fun getPlace(){

        val expectedPlace = Place(1L,
            "name",
            20.1,
            19.8,
            "description",
            "realName",
            "imageName",
            "category")


        placesViewModel.setPlace(expectedPlace)

        val place = placesViewModel.getPlace(expectedPlace.id!!)

        assertEquals(expectedPlace, place)

    }


    @Test fun separateNameFromPath(){
        val completePath = "/storage/emulated/0/Where/WherePhotos/IMAGE20183612103617.jpeg"
        val expected = Pair("/storage/emulated/0/Where/WherePhotos","IMAGE20183612103617.jpeg")
        val result = placesViewModel.separateNameFromPath(completePath)

        assertEquals(expected, result)
    }

}
